// Initialize tooltips
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

// Hide submenus
$(function() {
  $('#body-row .collapse').collapse('hide');

  // Collapse/Expand
  $('#collapse-icon').addClass('fa-angle-double-left');

  // Collapse click
  $('[data-toggle=sidebar-collapse]').click(function() {
    SidebarCollapse();
  });
});

function SidebarCollapse () {
  $('.menu-collapsed').toggleClass('d-none');
  $('.sidebar-submenu').toggleClass('d-none');
  $('.submenu-icon').toggleClass('d-none');
  $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

  // Treating d-flex/d-none on separators with title
  var SeparatorTitle = $('.sidebar-separator-title');
  if ( SeparatorTitle.hasClass('d-flex') ) {
    SeparatorTitle.removeClass('d-flex');
  } else {
    SeparatorTitle.addClass('d-flex');
  }
  // Collapse/Expand icon
  $('#collapse-icon').toggleClass('fa-chevron-up fa-chevron-down');
}

// Initialize TineMCE
tinyMCE.init({
  mode : "specific_textareas",
  selector : ".tmce-editor",
  theme : "silver",
  entity_encoding: 'raw',
  plugins: 'table code',
  table_class_list: [
    {title: 'None', value: ''},
    {title: 'BS4 Default', value: 'table'},
    {title: 'BS4 Dark', value: 'table table-dark'},
    {title: 'BS4 Default Striped', value: 'table table-striped'},
    {title: 'BS4 Dark Striped', value: 'table table-dark table-striped'},
  ],
  table_style_by_css: false,
});

$(function(){
  // listen for clicks on the checkall checkboxes
  $(`table input[type="checkbox"][data-action="checkall"`).click(function (e) {
    // Get the parent table
    let table = this.closest(`table`);

    // Check whether the checkall checkbox is checked
    if(this.checked) {
      // checked, switch all checkboxes to checked
      $(`#${table.id} input[type=checkbox]`).each(function () {
        $(this).prop("checked", true);
      });
    }else{
      // not checked, switch all checkboxes to unchecked
      $(`#${table.id} input[type=checkbox]`).each(function () {
        $(this).prop("checked", false);
      });
    }
  });
});

// Listen for token registration submission
$(function() {
  $(`form[data-action="register-token"]`).submit(async function(e) {
    // Prevent page refresh
    e.preventDefault();

    // Get our form
    let form = $(`form[data-action="register-token"]`);

    // Get the token name
    let tokenName = form.find('input[name="token-name"]').val();

    // Initialize our registration request
    const tokenRegistration = new TokenRegistration();

    // Register our token
    const result = await tokenRegistration.register('/graphql', tokenName);

    // Display our result in a toast
    switch(result.data.registerToken.success) {
      case true:
        toastr.success(result.data.registerToken.message);
        break;
      case false:
        toastr.error(result.data.registerToken.message);
        break;
    }
  });
});

// Listen for login form submission
$(function() {
  let loginMethod;
  let redir;

  // get our redir location
  redir = $(`form[data-action="user-login"]`).find(`input[name="redir"]`).val();
  if(!redir || redir == '') redir = '/admin';

  $(`form[data-action="user-login"] button[data-method]`).click(function(e) {
    e.preventDefault();

    // Get the method we want to use
    loginMethod = $(e.currentTarget).data().method;

    // Continue submission of the form
    $(`form[data-action="user-login"]`).submit();
  });

  $(`form[data-action="user-login"]`).submit(async function(e) {
    // Prevent page refresh
    e.preventDefault();

    // Get our form
    let form = $(`form[data-action="user-login"]`);

    // Get our username
    let username = form.find('input[name="username"]').val();

    // Invoke the method we need for logins
    let resp;
    switch(loginMethod) {
      case 'token':
        const tokenLogin = new TokenLogin();
        resp = await tokenLogin.login('/graphql', username);
        if(!resp.data.loginToken.success) {
          toastr.error(resp.data.loginToken.message);
          break;
        }
        toastr.success(resp.data.loginToken.message);
        window.location.href = redir;
        break;
      case 'email':
        const emailLogin = new EmailLogin();
        resp = await emailLogin.getToken('/graphql', username);
        if(!resp.data.emailLogin.success) {
          toastr.error(resp.data.emailLogin.message);
          break;
        }
        toastr.success(resp.data.emailLogin.message);
        form.addClass('d-none');
        $(`form[data-action="email-login"]`).removeClass('d-none');
        break;
    }
  });

  $(`form[data-action="email-login"]`).submit(async function(e) {
    // Prevent page refresh
    e.preventDefault();

    // Get our username
    let username = $(`form[data-action="user-login"]`).find('input[name="username"]').val();

    // Get our code
    let code = $(`form[data-action="email-login"]`).find('input[name="code"]').val();

    // Check the code and login
    const emailLogin = new EmailLogin();
    let resp = await emailLogin.login('/graphql', username, code);

    // Check if authentication was a success
    // If not, show a toast
    if(!resp.data.emailLogin.success) {
      toastr.error(resp.data.emailLogin.message);
      return;
    }

    // Redirect the user
    toastr.success(resp.data.emailLogin.message);
    window.location.href = redir;
  });
});

// Listen for login cancelation (for email)
$(function() {
  $(`form[data-action="email-login"] button[type="reset"]`).on('click', () => {
    $(`form[data-action="user-login"]`).removeClass('d-none');
    $(`form[data-action="email-login"]`).addClass('d-none');
  });
});
