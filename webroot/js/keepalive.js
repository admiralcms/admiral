// Keep our session alive
$(function() {
  setInterval(function() {
    $.ajax({
      url: '/admin/keepalive',
      method: 'GET'
    });
  }, 30000);
});