<?php
  namespace Admiral\Admiral;

  class Asset {
    private static $_urls = ['scripts' => []];
    private static $_scripts = [];

    public static function urls() {
      return self::$_urls;
    }

    public static function scripts() {
      return self::$_scripts;
    }
  
    public static function script(string $url, array $options = []) {
      self::$_urls['scripts'][] = ['url' => $url, 'options' => $options];
    }

    public static function scriptStart() {
      ob_start();
    }

    public static function scriptEnd() {
      self::$_scripts[] = ob_get_clean();
    }
  }