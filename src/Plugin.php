<?php
  namespace Admiral\Admiral;

  use Admiral\Admiral\Command\UsersDefaultCommand;

  use Cake\Core\BasePlugin;
  use Cake\Core\PluginApplicationInterface;
  use Cake\Console\CommandCollection;

  class Plugin extends BasePlugin {
    public function console($commands): CommandCollection {
      // Load our parent commands
      parent::console($commands);

      // Load our own commands
      $commands->add('users default', UsersDefaultCommand::class);

      // Return all commands
      return $commands;
    }

    public function bootstrap(PluginApplicationInterface $app): void {
      // Add constants, load configuration defaults.
      // By default will load `config/bootstrap.php` in the plugin.
      parent::bootstrap($app);
    }

    public function routes($routes): void {
      // Add routes.
      // By default will load `config/routes.php` in the plugin.
      parent::routes($routes);
    }
  }
