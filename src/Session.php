<?php
  namespace Admiral\Admiral;

  use Cake\Routing\Router;

  class Session {
    public static function get() {
      return Router::getRequest()->getSession();
    }
  }