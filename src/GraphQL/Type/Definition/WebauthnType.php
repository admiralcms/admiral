<?php
  namespace Admiral\Admiral\GraphQL\Type\Definition;

  use Admiral\Admiral\GraphQL\Datasource\WebauthnDatasource;

  use Admiral\GraphQL\Types;
  use GraphQL\Type\Definition\{
    ObjectType,
    ResolveInfo
  };

  class WebauthnType {
    protected $webauthnDatasource;

    public function config() {
      return [
        'name' => 'Webauthn',
        'fields' => function() {
          return [
            'register' => [
              'type' => Types::get('TokenRegisterChallenge'),
              'description' => 'Challenge that the client must solve',
              'args' => [
                'tokenName' => Types::get('string'),
              ]
            ],
            'login' => [
              'type' => Types::get('TokenLoginChallenge'),
              'description' => 'Challenge that the client must solve',
              'args' => [
                'username' => Types::get('string'),
              ]
            ]
          ];
        },
        'resolveField' => function($project, $args, $context, ResolveInfo $info) {
          $method = 'resolve' . ucfirst($info->fieldName);
          if (method_exists($this, $method)) {
            // Initialize the Datasource if not done yet
            if(!$this->webauthnDatasource) $this->webauthnDatasource = new WebauthnDatasource();

            // Resolve the field and return the info
            return $this->{$method}($project, $args, $context, $info);
          } else {
            return $project->{$info->fieldName};
          }
        }
      ];
    }

    public function resolveRegister($project, $args) {
      $challenge = $this->webauthnDatasource->registrationChallenge($args);
      return $challenge->jsonSerialize();
    }

    public function resolveLogin($project, $args) {
      $challenge = $this->webauthnDatasource->loginChallenge($args);
      if(is_array($challenge)) return $challenge;
      return $challenge->jsonSerialize();
    }
  }