<?php
  namespace Admiral\Admiral\GraphQL\Type\Definition;

  use Admiral\GraphQL\Types;
  use GraphQL\Type\Definition\{
    ObjectType
  };

  class TokenLoginChallengeType {
    public function config() {
      return [
        'name' => 'TokenLoginChallenge',
        'fields' => function() {
          return [
            'rpId' => Types::get('string'),
            'allowCredentials' => Types::listOf(new ObjectType([
              'name' => 'allowCredentialsType',
              'description' => 'List of allowed tokens',
              'fields' => [
                'type' => [
                  'type' => Types::get('string'),
                  'description' => 'The type of this token'
                ],
                'id' => [
                  'type' => Types::get('string'),
                  'description' => 'The ID of this token encoded in Base64'
                ],
              ]
            ])),
            'userVerification' => [
              'type' => Types::get('string'),
              'description' => '',
            ],
            'userHandle' => [
              'type' => Types::get('string'),
              'description' => '',
            ],
            'challenge' => [
              'type' => Types::get('string'),
              'description' => 'Challenge bytes encoded in hexadecimal'
            ],
            'timeout' => [
              'type' => Types::get('int'),
              'description' => 'The time the client has to respond',
            ],
          ];
        }
      ];
    }
  }