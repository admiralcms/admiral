<?php
  namespace Admiral\Admiral\GraphQL\Type\Definition;

  use Admiral\GraphQL\Types;
  use GraphQL\Type\Definition\InputObjectType;

  class PublicKeyCredentialType {
    public function config() {
      return [
        'name' => 'PublicKeyCredential',
        'fields' => function() {
          return [
            'id' => [
              'type' => Types::get('string'),
              'description' => '',
            ],
            'rawId' => [
              'type' => Types::get('string'),
              'description' => '',
            ],
            'response' => new InputObjectType([
              'name' => 'WebauthnResponse',
              'fields' => [
                'attestationObject' => [
                  'type' => Types::get('string'),
                  'description' => '',
                ],
                'clientDataJSON' => [
                  'type' => Types::get('string'),
                  'description' => '',
                ],
                'authenticatorData' => [
                  'type' => Types::get('string'),
                  'description' => '',
                ],
                'signature' => [
                  'type' => Types::get('string'),
                  'description' => '',
                ],
                'userHandle'  => [
                  'type' => Types::get('string'),
                  'description' => '',
                ],
              ],
            ]),
            'type' => [
              'type' => Types::get('string'),
              'description' => ''
            ],
          ];
        }
      ];
    }
  }