<?php
  namespace Admiral\Admiral\Model\Entity;

  use Admiral\Admiral\Utility\Asset;
  use Cake\Auth\DefaultPasswordHasher;
  use Cake\ORM\TableRegistry;
  use Cake\Filesystem\File;
  use Cake\Core\Configure;

  trait UserTrait {
    protected function _setPassword($password) {
      if (strlen($password) > 0) {
        return (new DefaultPasswordHasher)->hash($password);
      }
    }

    public function hasRight(string $right){
      return collection($this->roles)->extract('permissions.{*}')->firstMatch(['name' => $right]) !== null;
    }

    public function hasRole(string $role){
      return collection($this->roles)->extract('name')->contains($role);
    }

    public function getRights() {
      $res = collection($this->roles)->extract('permissions.{*}')->toArray();
      return $res;
    }

    public function addRole(int $role) {
      $usersRolesTable = TableRegistry::get('Admiral/Admiral.UsersRoles');

      $userRole = $usersRolesTable->newEntity();
      $userRole->user_id = $this->id;
      $userRole->role_id = $role;
      return $usersRolesTable->save($userRole);
    }

    public function removeRole(int $role) {
      $usersRolesTable = TableRegistry::get('Admiral/Admiral.UsersRoles');

      $userRole = $usersRolesTable->find()->where(['user_id' => $this->id, 'role_id' => $role])->first();
      if($userRole) {
        return $usersRolesTable->delete($userRole);
      }

      return false;
    }

    public function fullName() {
      // Load the required table
      $usersDetailTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.UsersDetails');

      // Get the user's details
      $details = $usersDetailTable->findById($this->id)->first();

      // Build the name
      $name = '';
      if(!empty($details->firstname)) $name .= $details->firstname;
      if(!empty($details->lastname)) $name .= ' ' . $details->lastname;

      return $name;
    }

    public function avatar() {
      // Check for a user specific avatar
      // If it exists, return that
      $prefix = (!empty(Configure::read('App.imageBaseUrl'))) ? Configure::read('App.imageBaseUrl') . DS : '';
      $file = new File(WWW_ROOT . DS . $prefix . 'user-avatars' . DS . $this->username . '.webp');
      if($file->exists()) return Asset::imageUrl('user-avatars' . DS . $this->username . '.webp', ['fullBase' => true]);

      // Check for an app specific default avatar
      // If it exists, return that
      $file = new File(WWW_ROOT . DS . $prefix . 'default-avatar.webp');
      if($file->exists()) return Asset::imageUrl('default-avatar.webp', ['fullBase' => true]);

      // Return our own default avatar
      return Asset::imageUrl('Admiral/Admiral.default-avatar.webp', ['fullBase' => true, 'pathPrefix' => 'img/']);
    }
  }
