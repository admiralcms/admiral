<?php
namespace Admiral\Admiral\Model\Entity;

use Cake\ORM\Entity;

/**
 * LoginOtp Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $code
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \Admiral\Admiral\Model\Entity\User $user
 */
class LoginOtp extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'code' => true,
        'created' => true,
        'user' => true
    ];
}
