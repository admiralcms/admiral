<?php
namespace Admiral\Admiral\Model\Entity;

use Cake\ORM\Entity;

/**
 * AuthToken Entity
 *
 * @property int $id
 * @property string $series
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $expires
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class AuthToken extends Entity {
  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
    'token' => true,
    'created' => true,
    'modified' => true,
    'expires' => true,
    'user_id' => true,
    'user' => true
  ];

  /**
   * Fields that are excluded from JSON versions of the entity.
   *
   * @var array
   */
  protected $_hidden = [
    'token'
  ];
}
