<?php
namespace Admiral\Admiral\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersDetails Model
 *
 * @method \Admiral\Admiral\Model\Entity\UsersDetail get($primaryKey, $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail newEntity($data = null, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail[] newEntities(array $data, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\UsersDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 255)
            ->allowEmpty('firstname');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 255)
            ->allowEmpty('lastname');

        return $validator;
    }
}
