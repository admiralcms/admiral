<table id="userstable" class="table table-striped table-sm">
  <thead>
    <th><input type="checkbox" id="checkall"/></th>
    <th><?= __d('Admiral/Admiral','Username') ;?></th>
    <th><?= __d('Admiral/Admiral','Name') ;?></th>
    <th><?= __d('Admiral/Admiral','Email') ;?></th>
    <th><?= __d('Admiral/Admiral','Role(s)') ;?></th>
    <th><?= __d('Admiral/Admiral','Edit') ;?></th>                  
    <th><?= __d('Admiral/Admiral','Delete') ;?></th>
  </thead>
  <tbody>
    <?php foreach ($users as $user): ?>
      <tr>
        <td><input type="checkbox" class="checkthis"/></td>
        <td><?= h($user->username); ?></td>
        <td><?= h($user->users_detail->firstname); ?> <?= h($user->users_detail->lastname); ?></td>
        <td><?= h($user->email); ?></td>
        <td>
          <?php foreach($user->roles as $role): ?>
            <?= h($role->name); ?>
          <?php endforeach; ?>
        </td>
        <td>
          <?= $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'far fa-edit' , 'escape' => false]),
            [
              'plugin' => 'Admiral/Admiral',
              'controller' => 'Users',
              'action' => 'edit',
              'id' => $user->id
            ],
            [
              'class' => 'btn btn-primary btn-xs rounded-0',
              'escape' => false
            ]
          ); ?>
        </td>
        <td>
          <button class="btn btn-danger btn-xs rounded-0" data-title="Delete" data-toggle="modal" data-target="#delete" >
            <span class="far fa-trash-alt"></span>
          </button>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <!-- This is actually the footer -->
  <thead>
    <th><input type="checkbox" id="checkall"/></th>
    <th><?= __d('Admiral/Admiral','Username') ;?></th>
    <th><?= __d('Admiral/Admiral','Name') ;?></th>
    <th><?= __d('Admiral/Admiral','Email') ;?></th>
    <th><?= __d('Admiral/Admiral','Role(s)') ;?></th>
    <th><?= __d('Admiral/Admiral','Edit') ;?></th>                  
    <th><?= __d('Admiral/Admiral','Delete') ;?></th>
  </thead>  
</table>

<script>
  $(function(){
    // listen for clicks on the checkall checkboxes
    $("#userstable #checkall").click(function (e) {
      // Check whether the checkall checkbox is checked
      if(this.checked) {
        // checked, switch all checkboxes to checked
        $("#userstable input[type=checkbox]").each(function () {
          $(this).prop("checked", true);
        });
      }else{
        // not checked, switch all checkboxes to unchecked
        $("#userstable input[type=checkbox]").each(function () {
          $(this).prop("checked", false);
        });
      }
    });   
    $("[data-toggle=tooltip]").tooltip();
  });
</script>