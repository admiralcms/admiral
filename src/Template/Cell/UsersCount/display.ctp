<div class="text-center">
  <h2><?= h($data['total']); ?></h2>
  <small>
    <b><?= h($data['new']); ?></b> <?= __d('Admiral/Admiral','new user(s) in the last'); ?> <b><?= __d('Admiral/Admiral','Month'); ?></b>
  </small>
</div>