<style>
.fileinput-button input {
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  -ms-filter: 'alpha(opacity=0)';
  font-size: 200px !important;
  direction: ltr;
  cursor: pointer;
}
</style>

<form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
  <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
  <div class="row fileupload-buttonbar">
    <div class="col-lg-7">
      <button type="button" data-action="add-file" class="btn btn-success mb-2 rounded-0">
        <i class="fas fa-plus"></i>
        <span><?= __d('Admiral/Admiral','Add files'); ?></span>
      </button>
      <button type="button" data-action="upload-queue" class="btn btn-primary mb-2 rounded-0">
        <i class="fas fa-upload"></i>
        <span><?= __d('Admiral/Admiral','Start upload'); ?></span>
      </button>
      <button type="button" data-action="clear-queue" class="btn btn-danger mb-2 rounded-0">
        <i class="fas fa-trash-alt"></i>
        <span><?= __d('Admiral/Admiral','Clear queue'); ?></span>
      </button>
    </div>
    <!-- The global progress state -->
    <div class="col-lg-5 fileupload-progress fade">
      <!-- The global progress bar -->
      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
      </div>
      <!-- The extended global progress state -->
      <div class="progress-extended">&nbsp;</div>
    </div>
  </div>
  <!-- The table listing the files available for upload/download -->
  <table role="presentation" class="table table-striped">
    <thead>
      <tr class="d-flex">
        <th class="col-1"><?= __d('Admiral/Admiral','Preview'); ?></th>
        <th class="col-2"><?= __d('Admiral/Admiral','Filename'); ?></th>
        <th class="col"><?= __d('Admiral/Admiral','Filesize'); ?></th>
        <th class="col-6"><?= __d('Admiral/Admiral','Progress'); ?></th>
        <th class="col-1"><?= __d('Admiral/Admiral','Actions'); ?></th>
      </tr>
    </thead>
    <tbody class="files"></tbody>
  </table>
</form>


<script id="template-upload" type="text/x-handlebars-template">
  <tr class="d-flex" id="tr-{{id}}">
    <td class="col-1">
      <span class="preview">{{{preview}}}</span>
    </td>
    <td class="col-2 d-inline-block text-truncate d-inline-block">{{name}}</td>
    <td class="col">{{size}}</td>
    <td class="col-6">
      <div class="progress rounded-0">
        <div id="prog-{{id}}" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
    <td class="col-1">
      <button type="button" class="btn btn-danger rounded-0" data-action="remove-item" data-item="{{id}}">
        <i class="fas fa-trash-alt"></i>
      </button>
    </td>
  </tr>
</script>

<?php \Admiral\Admiral\Asset::scriptStart(); ?>
  <script>
    // Compile our Handlebars template
    var template = Handlebars.compile($("#template-upload").html());

    // Initialize the Resumable object
    var r = new Resumable({
      target:'<?= $this->Url->build(["controller" => "Media","action"=>"add"]); ?>',
      chunkSize: 0.5*1024*1024,
      headers: {
        'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
      }
    });

    // Assign the browse button
    r.assignBrowse($("button[data-action=add-file"));

    /**
    * Handler for when a file has been added to the queue
    */
    r.on('fileAdded', function(file){
      var fileReader = new FileReader();
      fileReader.readAsDataURL(file.file);

      fileReader.onload = function (event) {
        var context = {
          id: file.uniqueIdentifier,
          name: file.fileName,
          preview: () => {
            // Check which filetype we're dealing with and build the preview HTML
            type = fileReader.result.match(/data:([a-zA-Z0-9]+)\/[a-zA-Z0-9-.+]+.*,.*/)[1];
            switch(type){
              case "image":
                preview = `<img src="${fileReader.result}" style="max-height: 50px">`;
                break;
              default:
                preview = `<i class="fas fa-file fa-3x"></i>`;
                break;
            }

            // Return the preview HTML
            return preview;
          },
          size: () => {
            // Check if the file has a size of 0, and return 0 Bytes as a default
            if(file.size == 0) return '0 Bytes';

            // Set the division unit
            var k = 1000;

            // Define our units
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

            // Do some math stuff
            var i = Math.floor(Math.log(file.size) / Math.log(k));
            return parseFloat((file.size / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
          }
        };

        $("#fileupload table>tbody.files").append(template(context));
      };
    });

    /**
    * Click handler for removing specific items from the queue
    */
    $(document).on('click', "button[data-action=remove-item]", function(){
      var file = r.getFromUniqueIdentifier($(this).data("item"));
      file.cancel();

      $("#tr-" + $(this).data("item")).slideUp(300, function(){
        $(this).remove();
      });
    });

    /**
    * Click handler for clearing the entire queue
    */
    $(document).on('click', "button[data-action=clear-queue]", function() {
      r.cancel();
      $("#fileupload table>tbody.files>tr").slideUp(300, function(){
        $(this).remove();
      });
    });

    /**
    * Click handler for uploading the queue
    */
    $(document).on('click', "button[data-action=upload-queue]", function() {
      r.upload();
    });

    /**
    * Progress handler
    */
    r.on('fileProgress', function(file){
      $(`#prog-${file.uniqueIdentifier}`).css("width", (file.progress() * 100) + "%");
      $(`#prog-${file.uniqueIdentifier}`).html((file.progress() * 100) + "%");
    });

    /**
    * Completion handler
    */
    r.on('fileSuccess', function(file) {
      $(`#prog-${file.uniqueIdentifier}`).addClass("bg-success");
      $(`#prog-${file.uniqueIdentifier}`).text("<?= __d('Admiral/Admiral','File uploaded successfully!'); ?>");

      $(`button[data-action=delete-item][data-item=${file.uniqueIdentifier}]`).remove();
    });

    /**
    * Failure handler
    */
    r.on('fileError', function(file, message) {
      $(`#prog-${file.uniqueIdentifier}`).addClass("bg-danger");
      $(`#prog-${file.uniqueIdentifier}`).text(message);

      $(`button[data-action=delete-item][data-item=${file.uniqueIdentifier}]`).remove();
    });
  </script>
<?php \Admiral\Admiral\Asset::scriptEnd(); ?>