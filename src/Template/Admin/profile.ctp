<div class="row">
  <div class="col-md-3">
    <h4><?= __d('Admiral/Admiral','Your Details'); ?></h4>
    <?= $this->Flash->render('userDetails'); ?>

    <?= $this->Form->create($userDetailsForm); ?>
      <div class="form-group row">
        <div class="col-6">
          <?php
            $isValid = ""; # Clear our variable
            // Check whether this form field is valid or not
            if(!empty($validationErrors['firstname'])){
              // Form field has an error
              $isValid = "is-invalid";
            }
          ?>
          <?= $this->Form->control("firstname",["placeholder"=>__d('Admiral/Admiral',"Your First Name"),"label"=>false,"class"=>"form-control rounded-0 " . $isValid,"value" => $userData['users_detail']['firstname']]); ?>
          <?php
            if(!empty($validationErrors['firstname'])){
              foreach($validationErrors['firstname'] as $error){
                ?>
                  <div class="invalid-feedback d-block"><?= $error; ?></div>
                <?php
              }
            }
          ?>
        </div>
        <div class="col-6">
          <?php
            $isValid = ""; # Clear our variable
            // Check whether this form field is valid or not
            if(!empty($validationErrors['lastname'])){
              // Form field has an error
              $isValid = "is-invalid";
            }
          ?>
          <?= $this->Form->control("lastname",["placeholder"=>__d('Admiral/Admiral',"Your Last Name"),"label"=>false,"class"=>"form-control rounded-0 " . $isValid,"value" => $userData['users_detail']['lastname']]); ?>
          <?php
            if(!empty($validationErrors['lastname'])){
              foreach($validationErrors['lastname'] as $error){
                ?>
                  <div class="invalid-feedback d-block"><?= $error; ?></div>
                <?php
              }
            }
          ?>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-12">
          <?= $this->Form->button('Submit',["class"=>"btn btn-success rounded-0","value"=>__d('Admiral/Admiral','Submit')]); ?>
          <?= $this->Form->button('Reset',["class"=>"btn btn-danger rounded-0","value"=>__d('Admiral/Admiral','Reset'),"type"=>"reset"]); ?>
        </div>
      </div>
      <?= $this->Form->hidden('action',["value" => "userDetails"]); ?>
    <?= $this->Form->end(); ?>
  </div>

  <!-- Security Tokens -->
  <div class="col-md-3">
    <h4><?= __d('Admiral/Admiral','Security Tokens'); ?></h4>
    <!-- New token registration -->
    <form data-action="register-token">
      <div class="input-group mb-3">
      <input type="text" class="form-control rounded-0" placeholder="Enter a token name" name="token-name">
        <div class="input-group-append">
          <button class="btn btn-primary rounded-0" type="submit"><i class="fas fa-plus"></i></button>
        </div>
      </div>
    </form>

    <!-- Display all keys -->
    <?= $this->Ui->table->start(); ?>
      <?= $this->Ui->table->header([
        'Token Name',
        'Actions'
      ]); ?>
      <?php foreach($authenticationKeys as $name => $data): ?>
        <?= $this->Ui->table->rowStart(); ?>
          <td data-toggle="tooltip" data-placement="bottom" title="<?= $this->WebauthnKey->fingerprint($data->getPublicKeyCredentialId()); ?>">
            <?= h($name); ?>
          </td>
          <td>
            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
              <span class="far fa-trash-alt"></span>
            </button>
          </td>
        <?= $this->Ui->table->rowEnd(); ?>
      <?php endforeach; ?>
    <?= $this->Ui->table->end(); ?>
  </div>

  <div class="col-md-2">
    <h4><?= __d('Admiral/Admiral','Avatar'); ?></h4>
    <img src="<?= h($user->avatar()); ?>" style="max-width: 250px; max-height: 250px;" class="mb-2" id="avatar-preview">
    <form data-action="change-avatar">
      <button type="button" data-action="change-avatar" class="btn btn-primary rounded-0">
        <i class="fas fa-plus"></i>
        <span><?= __d('Admiral/Admiral','Change avatar'); ?></span>
      </button>
      <button type="button" data-action="submit-avatar" class="btn btn-success rounded-0 d-none">
        <i class="fas fa-plus"></i>
        <span><?= __d('Admiral/Admiral','Submit'); ?></span>
      </button>
      <br />
      <small>Images can be up-to 5MB</small>

      <input type="file" name="new-avatar" accept="image/jpeg, image/png, image/webp" style="opacity: 0">
    </form>
  </div>
</div>

<?php \Admiral\Admiral\Asset::scriptStart(); ?>
  <script>
    let avatarData = '';
    $(`button[data-action="change-avatar"]`).on('click', (e) => {
      // Stop form submission
      e.preventDefault();

      // Click the file input
      $(`form[data-action="change-avatar"] input[name="new-avatar"]`).trigger('click');
    });

    $(`form[data-action="change-avatar"] input[name="new-avatar"]`).on('change', async (e) => {
      let reader = new FileReader();
      reader.onload = function(data) {
        // Make sure we don't go over the filesize limit
        if(data.loaded >= (5 * 1024 * 1024)) {
          toastr.error(`It appears your new avatar is too big...<br />Please stick to <b>5MB</b> or smaller!`, 'Whoops!');
          return;
        }

        $(`#avatar-preview`).attr('src', data.target.result);
        avatarData = data.target.result;
        $(`button[data-action="submit-avatar"]`).removeClass('d-none');
        toastr.success(`Avatar has been loaded!<br />you can preview it below to make sure it looks good!`, 'Hoozay!');
      }

      reader.readAsDataURL(e.currentTarget.files[0]);
    });

    $(`button[data-action="submit-avatar"]`).on('click', async (e) => {
      // Initialize a new GraphQL query
      const gql = new GraphQL('/graphql');

      // Add our mutation
      gql.setQuery(`mutation($data: String) {
        updateAvatar(data: $data) {
          success
          message
        }
      }`);

      // Add our variables
      gql.addVariable('data', avatarData);

      // Execute our mutation
      const resp = await gql.execute();

      if(resp.data.updateAvatar.success) {
        toastr.success(resp.data.updateAvatar.message, 'Hoozay!');
        $(`button[data-action="submit-avatar"]`).addClass('d-none');
      } else {
        toastr.error(resp.data.updateAvatar.message, 'Whoops!');
      }
    });
  </script>
<?php \Admiral\Admiral\Asset::scriptEnd(); ?>
