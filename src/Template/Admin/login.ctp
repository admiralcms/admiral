<div class="container py-5">
  <div class="row">
    <div class="col-md-12">
      <h2 class="text-center mb-4">
        <img src="https://via.placeholder.com/1920x1080" class="d-inline-block align-top" alt="" style="max-width: 256px; max-height: 128px;">
      </h2>
      <div class="row">
        <div class="col-md-6 mx-auto">
          <?= $this->Flash->render(); ?>
          <div class="card rounded-0">
            <div class="card-header">
              <h3 class="mb-0"><?= __d('Admiral/Admiral','Login'); ?></h3>
            </div>
            <div class="card-body">
              <form data-action="user-login">
                <div class="form-group">
                  <?= $this->Form->label(__d('Admiral/Admiral','Username')); ?>
                  <?= $this->Form->control("username",["placeholder"=>__d('Admiral/Admiral','Enter your username'),"required"=>true,"label"=>false,"class"=>"form-control form-control-lg rounded-0"]); ?>
                  <div class="invalid-feedback"><?= __d('Admiral/Admiral','Please enter your username'); ?></div>
                </div>
                <div class="form-group">
                  <?= $this->Form->checkbox('remember_me'); ?>
                  <label for="remember_me"><?= __d('Admiral/Admiral', 'Remember Me'); ?></label>
                </div>
                <?= $this->Form->hidden('redir', ["value" => $this->request->getQuery('redir')]); ?>
                <div class="form-group">
                  <label><?= __d('Admiral/Admiral', 'Login with'); ?>...</label>
                  <br />
                  <button type="submit" class="btn btn-success btn-lg rounded-0" data-method="token"><?= __d('Admiral/Admiral','Security Token'); ?></button>
                  <button type="submit" class="btn btn-primary btn-lg rounded-0" data-method="email"><?= __d('Admiral/Admiral','Email'); ?></button>
                </div>
              </form>
              <form data-action="email-login" class="d-none">
                <div class="form-group">
                  <?= $this->Form->control("code",["placeholder"=>__d('Admiral/Admiral','Enter your login code'),"required"=>true,"label"=>false,"class"=>"form-control form-control-lg rounded-0"]); ?>
                  <div class="invalid-feedback"><?= __d('Admiral/Admiral','Enter your login code'); ?></div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-lg rounded-0"><?= __d('Admiral/Admiral','Login'); ?></button>
                  <button type="reset" class="btn btn-danger btn-lg rounded-0"><?= __d('Admiral/Admiral','Cancel'); ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>