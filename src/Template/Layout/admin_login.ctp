<!DOCTYPE html>
<html>
  <head>
    <!-- Page meta -->
    <?= $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
      Login - <?= h(\Cake\Core\Configure::read('Admiral.branding.name') ?? 'Admiral'); ?>
    </title>

    <!-- Stylesheets -->
    <?= $this->Html->css('Admiral/Admiral.vendor/bootstrap/bootstrap.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.vendor/fontawesome/fontawesome.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.styles.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.vendor/toastr/toastr.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.vendor/flag-icon/flag-icon.css'); ?>
  </head>
  <body class="bg-light">
    <?= $this->fetch('content'); ?>

    <!-- Scripts -->
    <?php foreach(\Admiral\Admiral\Asset::urls()['scripts'] as $script): ?>
      <?= $this->Html->script($script['url'], $script['options']); ?>
    <?php endforeach; ?>

    <?php foreach(\Admiral\Admiral\Asset::scripts() as $script): ?>
      <?= $script; ?>
    <?php endforeach; ?>
  </body>
</html>