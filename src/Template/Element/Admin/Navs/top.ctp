<nav class="navbar navbar-expand navbar-dark bg-dark fixed-top">
  <a class="sidebar-toggle mr-3" href="#"><span class="navbar-toggler-icon"></span></a>
  <a class="navbar-brand font-weight-bold" href="/admin"><?= h(\Cake\Core\Configure::read('Admiral.branding.name') ?? 'Admiral'); ?></a>

  <div class="navbar-collapse collapse">
    <ul class="navbar-nav ml-auto">
      <!-- Back to site link -->
      <li class="nav-item">
        <?= $this->Html->link(__d('Admiral/Admiral','Back to site'), ['plugin' => null, 'controller' => 'Pages', 'action' => 'display', 'home'],['class' => 'nav-link']); ?>
      </li>

      <!-- Language selection -->
      <li class="nav-item dropdown">
        <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <?= $this->Html->image(
            'Admiral/Admiral./flags/4x3/' . $countryLocale . '.svg',
            [
              'fullBase' => true,
              'width' => 40,
              'height' => 30
            ]
          ); ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="<?= $this->request->getAttribute("here"); ?>?lang=en_GB" class="dropdown-item">
            <img src="<?= $this->Url->image('Admiral/Admiral./flags/4x3/gb.svg', ['fullBase' => true]); ?>" width="25px" height="15px"> English
          </a>
          <a href="<?= $this->request->getAttribute("here"); ?>?lang=nl_NL" class="dropdown-item">
            <img src="<?= $this->Url->image('Admiral/Admiral./flags/4x3/nl.svg', ['fullBase' => true]); ?>" width="25px" height="15px"> Dutch (incomplete)
          </a>
        </div>
      </li>

      <!-- User dropdown -->
      <li class="nav-item dropdown">
        <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <img width="30" height="30" class="d-inline-block align-top" src="<?= h($user->avatar()); ?>">
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd_user">
          <a href="#" class="dropdown-item">
            <i class="fas fa-user"></i> <?= h($user->fullName()); ?>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?= $this->Url->build(["plugin"=>'Admiral/Admiral',"controller" => "Users","action"=>"profile"]); ?>" class="dropdown-item">
            <i class="fas fa-id-card"></i> <?= h(__d('Admiral/Admiral','Profile settings')); ?>
          </a>
          <a href="<?= $this->Url->build(["plugin"=>'Admiral/Admiral',"controller"=>"Users","action"=>"logout"]); ?>" class="dropdown-item">
            <i class="fas fa-sign-out-alt"></i> <?= h(__d('Admiral/Admiral','Sign out')); ?>
          </a>
        </div>
      </li>
    </ul>
  </div>
</nav>
