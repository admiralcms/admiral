<?php
namespace Admiral\Admiral;

use Cake\Utility\Inflector;
use Cake\Utility\Hash;
use Admiral\Admiral\Permission;
use Admiral\Admiral\User;
use Cake\Collection\Collection;

class Menu {
  public static $_items = [];

  public static function add(string $section, array $item) {
    self::_registerSection([
      'label' => Inflector::humanize($section),
      'name' => $section
    ]);

    $item['section'] = $section;

    self::_registerItem($item);
  }

  protected static function _registerSection(array $item) {
    if(!array_key_exists($item['name'], self::$_items)) {
      self::$_items[$item['name']] = [
        'label'=> $item['label'],
        'items'=> []
      ];
    }
  }

  protected static function _registerItem(array $item) {
    self::$_items[$item['section']]['items'][$item['name']] = [
      'label' => $item['label'],
      'icon' => $item['icon'],
      'url' => (empty($item['url']) ? '#' : $item['url']),
      'children' => (empty($item['children']) ? [] : $item['children']),
      'weight' => (!isset($item['weight']) ? 9999 : $item['weight']),
      'permissions' => (empty($item['permissions']) ? [] : $item['permissions']),
    ];
  }

  public static function addChild(string $section, string $parent, array $child){
    self::$_items[$section]['items'][$parent]['children'][] = $child;
  }

  public static function getItems($sort = true) {
    // Remove items the user has no permission to
    self::_filterPermissionless();

    if($sort) {
      self::_sortItems();
    }
    
    return self::$_items;
  }

  private static function _filterPermissionless() {
    foreach(self::$_items as &$section) {
      $col = collection($section['items']);

      // Filter the main items first
      $res = $col->filter(function($value) {
        if(!empty($value['permissions'])) {
          $res = self::_checkPermission($value['permissions']);
          if(!$res) {
            // Remove the item
            return false;
          }
        }

        // Keep the item
        return true;
      });

      // Filter the children
      $res = $res->map(function($value) {
        if(empty($value['children'])) {
          return $value;
        }

        foreach($value['children'] as $index => $child) {
          if(empty($child['permissions'])) {
            return $value;
          }

          $hasPermission = self::_checkPermission($child['permissions']);
          if(!$hasPermission) {
            unset($value['children'][$index]);
          }

          return $value;
        }
      });
      $section['items'] = $res->toArray();
    }
  }

  private static function _sortItems() {
    foreach(self::$_items as &$section) {
      $section['items'] = Hash::sort($section['items'], '{*}.weight', 'asc', 'natural');
    }
  }

  private static function _checkPermission($permissions) {
    foreach($permissions as $permission) {
      if(Permission::check($permission) != 1) {
        return false;
      }
    }

    return true;
  }
}