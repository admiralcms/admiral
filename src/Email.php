<?php
  namespace Admiral\Admiral;

  use Cake\Mailer\Email as CakeMail;

  class Email {
    private $email = [];
    private $cakeMail;

    public function __construct() {
      $this->cakeMail = new CakeMail();
      $this->cakeMail->setTransferEncoding('8bit');
      $this->email['transport'] = 'default';
      $this->email['template'] = 'default';
      $this->email['from'] = $this->cakeMail->getConfigTransport('default')['from'];
    }

    public function set(string $option = '', $value) {
      $this->email[$option] = $value;
    }

    public function get(string $option = null) {
      if(!$option) {
        return $this->email;
      }

      return $this->email[$option];
    }

    public function send() {
      // Create the mail
      $this->cakeMail->transport($this->email['transport']);

      $this->cakeMail->setTo($this->email['to']);
      $this->cakeMail->setFrom($this->email['from']['mail'], $this->email['from']['name']);
      $this->cakeMail->setSender($this->email['from']['mail']);

      if(!empty($this->email['subject'])) {
        $subject = !empty(env('ADMIRAL_EMAIL_PREFIX')) ? '[' . env('ADMIRAL_EMAIL_PREFIX') . '] ' : '';
        $subject .= $this->email['subject'];
        $this->cakeMail->setSubject($subject);
      }
      if(!empty($this->email['replyTo'])) $this->cakeMail->setReplyTo($this->email['replyTo']);
      if(!empty($this->email['template'])) $this->cakeMail->viewBuilder()->setTemplate($this->email['template']);
      if(!empty($this->email['format'])) $this->cakeMail->emailFormat($this->email['format']);
      if(!empty($this->email['viewVars'])) $this->cakeMail->setViewVars($this->email['viewVars']);

      // Send the email and return the result
      return $this->cakeMail->send();
    }
  }