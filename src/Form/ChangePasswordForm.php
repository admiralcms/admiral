<?php
  namespace Admiral\Admiral\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;
  use Cake\Auth\DefaultPasswordHasher;

  class ChangePasswordForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('currentPassword', ['type' => 'string'])
        ->addField('newPassword', ['type' => 'string'])
        ->addField('newPasswordConfirm',['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the currentPassword field
      ->requirePresence('currentPassword')
      ->notEmpty('currentPassword', 'Please enter your current password')
      // Rules for the newPassword field
      ->requirePresence('newPassword')
      ->notEmpty('newPassword', 'Please enter your new password')
      // Rules for the newPassword field
      ->requirePresence('newPasswordConfirm')
      ->notEmpty('newPasswordConfirm', 'Please confirm your new password')
      // Check whether the newPassword and newPasswordConfirm match
      ->add('newPasswordConfirm', [
        'compare' => [
          'rule' => ['compareWith', 'newPassword'],
          'message' => 'New password and confirmation do not match!'
        ]
      ]);
      return $validator;
    }

    protected function _execute(array $data) {
      return true;
    }

    public function setErrors($errors){
      $this->_errors = $errors;
    }

    public function checkPassword($inputPassword,$user){
      return (new DefaultPasswordHasher)->check($inputPassword,$user->password);
    }
  }
