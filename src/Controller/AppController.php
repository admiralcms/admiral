<?php
  namespace Admiral\Admiral\Controller;

  use App\Controller\AppController as BaseController;
  use Cake\Core\Configure;
  use Cake\Http\Cookie\Cookie;
  use Cake\I18n\{
    Time,
    I18n
  };
  use Admiral\Admiral\Menu;
  use Admiral\Admiral\User;
  use Admiral\Admiral\Asset;
  use Admiral\Admiral\PackageInfo;

  class AppController extends BaseController {
    protected $smtpOptions;

    public function initialize(): void {
      parent::initialize();

      $this->loadComponent('Auth');
      PackageInfo::load();
      PackageInfo::selectPlugin('admiral/admiral');

      // Load required models
      $this->loadModel('Admiral/Admiral.Users');
      $this->loadModel('Admiral/Admiral.Options');
      
      // Get the localization selected by the user
      if($this->request->getQuery('lang')) {
        $cookie = (new Cookie('Config_locale'))
            ->withValue($this->request->getQuery('lang'))
            ->withExpiry(new Time('+1 year'));
          $this->response = $this->response->withCookie($cookie);
        I18n::setLocale($this->request->getQuery('lang'));
      } else {
        if($this->request->getCookie('Config_locale')) {
          I18n::setLocale($this->request->getCookie('Config_locale'));
        } else {
          $cookie = (new Cookie('Config_locale'))
            ->withValue('en_GB')
            ->withExpiry(new Time('+1 year'));
          $this->response = $this->response->withCookie($cookie);
          I18n::setLocale('en_GB');
        }
      }

      $shortLocale = strtolower(explode("_",I18n::getLocale())[1]);
      $this->set('countryLocale', $shortLocale);

      $this->set('user', User::get());
      
      $this->set('menuSections',Menu::getItems(User::get()));
    }
  }