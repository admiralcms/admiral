<?php
  namespace Admiral\Admiral\Controller;

  use Admiral\Admiral\Controller\Base\UsersBaseController;
  use Admiral\Admiral\Permission;
  use Admiral\Admiral\Form\{
    UserDetailsForm,
    ChangePasswordForm
  };
  use Cake\Core\Configure;

  class UsersController extends UsersBaseController {
    public function initialize(): void {
      parent::initialize();

      $this->loadModel('Admiral/Admiral.Users');
      $this->loadModel('Admiral/Admiral.Roles');

      $this->viewBuilder()->setLayout('admin');
    }

    public function index() {
      // Retrieve all users and their data
      $users = $this->Users->find('all')->contain(['UsersDetails','Roles']);

      $this->set('title', __d('Admiral/Admiral','Users'));
      $this->set(compact('users'));
      
    }

    public function edit($id) {
      $this->set('title', __d('Admiral/Admiral','Edit User'));
      
      parent::myAccount($id);

      if($this->request->is('post')) {
        if($this->request->getData('action') == 'removeRole') {
          if($this->request->getData('role') == 2) {
            return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'failure',
                'message' => 'Cannot remove mandatory role "Member"'
              ]));
          }

          $rolename = $this->Roles->findById($this->request->getData('role'))->first();
          if(!$rolename) {
            return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'failure',
                'message' => 'Role with id "'.$this->request->getData('role').'" does not exist'
              ]));
          }

          $userData = $this->Users
            ->find()
            ->where(['Users.id' => $id])
            ->contain(['UsersDetails','Roles'])
            ->first();

          if($userData->removeRole($this->request->getData('role'))) {
            return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'success',
                'message' => 'User has been removed from role "'.$rolename->name.'"'
              ]));
          }
        }

        if($this->request->getData('action') == 'addRole') {
          $rolename = $this->Roles->findById($this->request->getData('role'))->first();
          if(!$rolename) {
            return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'failure',
                'message' => 'Role with id "'.$this->request->getData('role').'" does not exist'
              ]));
          }

          $userData = $this->Users
            ->find()
            ->where(['Users.id' => $id])
            ->contain(['UsersDetails','Roles'])
            ->first();

          if($userData->hasRole($rolename->name)) {
            return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'failure',
                'message' => 'User already is a member of role "'.$rolename->name.'"'
              ]));
          }

          if(!$userData->addRole($this->request->getData('role'))) {
            return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'failure',
                'message' => 'Could not add user to role "'.$rolename->name.'"'
              ]));
          }

          return $this->response
              ->withType('application/json')
              ->withStringBody(json_encode([
                'status' => 'success',
                'message' => 'User has been added to role "'.$rolename->name.'"'
              ]));
        }
      }

      
      $this->set('roles', Permission::roles());
    }

    public function login(){
      if($this->Auth->user('id')) {
        return $this->redirect(["action" => "index"]);
      }

      parent::login();
      
      $this->viewBuilder()->setLayout('admin_login');
      $this->render('Admin/login');
    }

    public function profile() {
      parent::myAccount($this->Auth->user('id'));

      $this->set('title', __d('Admiral/Admiral','My Profile'));
      $this->viewBuilder()->setLayout('admin');
      $this->render('Admin/profile');
    }

    public function getrights($id) {
      if(!$this->Auth->user('id')) {
        return $this->response
          ->withType('application/json')
          ->withStringBody(json_encode([
            'status' => 'failure',
            'message' => 'You are not allowed to access this method'
          ]));
      }

      $userData = $this->Users
        ->find()
        ->where(['Users.id' => $id])
        ->contain(['UsersDetails','Roles'])
        ->first();

      if(!$userData) {
        return $this->response
          ->withType('application/json')
          ->withStringBody(json_encode([
            'status' => 'failure',
            'message' => 'User does not exist'
          ]));
      }

      $rights = [];
      dd($userData);
      foreach($userData->roles as $role) {
        dd($role);
      }

      ksort($rights);

      return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'status' => 'success',
          'message' => '',
          'result' => $rights
        ]));
    }
  }