<?php
  namespace Admiral\Admiral\Controller\Component;

  use Cake\Controller\Component;

  class SecurityHeaderComponent extends Component {
    private $permissionsPolicy = [];
    private $contentPolicy = [];
    private $hstsHeader = '';

    /**
     * Enable the "Strict-Transport-Security" header.
     * Enabling this header will tell the client to enable HSTS (if supported).
     *
     * @param bool $enabled Whether to enable HSTS
     * @param bool $includeSubDomains Whether to include subdomains in the HSTS policy
     * @param bool $preload Whether to check the hardcoded HSTS list of the client
     * @param int $expiry Expiry time in seconds
     * @return void
     */
    public function setHsts(bool $enabled = true, bool $includeSubDomains = false, bool $preload = true, int $expiry = 63072000): void {
      // Make sure we've enabled HSTS
      if(!$enabled) {
        $this->addHeader('Strict-Transport-Security', 'max-age=0');
        return;
      }

      // Make sure expiry is an integer and bigger than 0
      // If not, set it to the default
      if(!is_int($expiry)) $expiry = 63072000;
      if($expiry <= 0) {
        trigger_error('Expiry cannot be 0 or lower, to disable HSTS, please set "$enabled" to false instead. Setting to 63072000 for now.', E_WARNING);
        $expiry = 63072000;
      }

      // Set our max age to 2 years
      $this->hstsHeader .= 'max-age=' . $expiry;

      // Check if we need to include subdomains
      // If so, add this to the header
      if($includeSubDomains) $this->hstsHeader .= '; includeSubDomains';

      // Check if we need to preload
      // If so, ad this to the header
      if($preload) $this->hstsHeader .= '; preload';

      // Add our header to the response
      $this->addHeader('Strict-Transport-Security', $this->hstsHeader);
    }

    /**
     * Add directives to the "Permissions-Policy" header.
     * Policies are only enforced if the SecurityHeaderComponent#getPermissions method is called.
     *
     * @param string $permission The permission directive to use
     * @param string $host The origin on which to allow this directive
     * @return $this
     */
    public function setPermission(string $permission, string $host = ''): self {
      // Make sure a permission is set
      // If not, return
      if(empty($permission)) return $this;

      // Check if the permission exists
      // If not, create it
      if(!array_key_exists($permission, $this->permissionsPolicy)) $this->permissionsPolicy[$permission] = [];

      // Set our permission
      if(!empty($host)) $this->permissionsPolicy[$permission][] = $host;

      return $this;
    }

    /**
     * Sets the "Referrer-Policy" header.
     *
     * @param string $default The default referrer policy to enforce
     * @param string $fallback The fallback referrer policy to enfore if the default is not supported
     * @return void
     */
    public function setReferrerPolicy(string $default = 'strict-origin', string $fallback = 'strict-origin-when-cross-origin'): void {
      // Make sure a default is set
      // If not, set to "strict-origin"
      if(empty($default)) $default = 'strict-origin';

      // Make sure a fallback is set
      // If not, set to "strict-origin-when-cross-origin"
      if(empty($fallback)) $fallback = 'strict-origin-when-cross-origin';

      // Add our header to the response
      $this->addHeader('Referrer-Policy', $default . ', ' . $fallback);
    }

    /**
     * Sets the "X-Frame-Options" header to inform the client whether the document is embeddable.
     * Supports "SAMEORIGIN" and "DENY" as $setting param.
     *
     * @param string $setting
     * @return void
     */
    public function setXFrame(string $setting = 'SAMEORIGIN'): void {
      // Make sure the header is set to one of the only allowed values
      if(!in_array(strtoupper($setting), ['SAMEORIGIN', 'DENY'])) return;

      // Add our header to the response
      $this->addHeader('X-Frame-Options', strtoupper($setting));
    }

    /**
     * Set the "X-Content-Type-Options" to "nosniff" to disable MIME sniffing by the client.
     *
     * @param bool $enabled Whether to set the header to "nosniff"
     * @return void
     */
    public function sniffMime(bool $enabled = true): void {
      if($enabled) $this->addHeader('X-Content-Type-Options', 'nosniff');
    }

    /**
     * Add directives to the "Content-Security-Policy" header.
     * Policies are only enforced if the SecurityHeaderComponent#getContentPolicies method is called.
     *
     * @param string $directive The directive to add
     * @param string $host The origins from which to allow the directive
     * @return $this
     */
    public function setCsp(string $directive, string $host = ''): self {
      // Make sure a directive is set
      // If not, return
      if(empty($directive)) return $this;

      // Check if the policy exists
      // If not, create it
      if(!array_key_exists($directive, $this->contentPolicy)) $this->contentPolicy[$directive] = [];

      // Check if our host equal "self" or "none"
      // If so, add brackets around them
      $needBrackets = ['self', 'none', 'unsafe-eval', 'unsafe-hashed', 'unsafe-inline'];
      if(in_array(strtolower($host), $needBrackets)) $host = '\'' . $host . '\'';

      // Set our policy
      if(!empty($host)) $this->contentPolicy[$directive][] = $host;

      return $this;
    }

    /**
     * Sets the "Permissions-Policy" header.
     * Should be ran once all permissions have been set using SecurityHeaderComponent#setPermission.
     *
     * @return void
     */
    public function getPermissions(): void {
      // Get our last policy
      $lastPolicy = array_key_last($this->permissionsPolicy);

      // Loop over each permission set
      // Add them to our policies string
      $policies = '';
      foreach($this->permissionsPolicy as $policy => $hosts) {
        // Start the policy
        $policies .= $policy;
        $policies .= '=(';

        // Add our hosts
        $policies .= $this->addHosts($hosts);

        // End out policy
        $policies .= ')';

        // Check if this was the last policy
        // If not, add a ","
        if($policy !== $lastPolicy) $policies .= ',';
      }

      // Update our header
      $this->addHeader('Permissions-Policy', $policies);
    }

    /**
     * Sets the "Content-Security-Policy" header.
     * Should be ran once all permissions have been set using SecurityHeaderComponent#setCsp.
     *
     * @return void
     */
    public function getContentPolicies(): void {
      // Get our last policy
      $lastPolicy = array_key_last($this->contentPolicy);

      // Loop over each permission set
      // Add them to our policies string
      $policies = '';
      foreach($this->contentPolicy as $policy => $hosts) {
        // Start the policy
        $policies .= $policy;
        $policies .= ' ';

        // Add our hosts
        $policies .= $this->addHosts($hosts);

        // End our policy line
        $policies .= ';';
      }

      // Update our header
      $this->addHeader('Content-Security-Policy', $policies);
    }

    /**
     * Adds the header to the response the parent controller will send out
     *
     * @param string $header The header name key to set
     * @param string $value The value to which to set the header
     * @return void
     */
    private function addHeader(string $header, string $value = ''): void {
      // Make sure a header is set and not null
      if(empty($header)) return;

      // Update the response
      $response = $this->getController()->getResponse();
      $response = $response->withAddedHeader($header, $value);
      $this->getController()->setResponse($response);
    }

    /**
     * Loops over each host in $hostsList and adds them to a string (seperated by spaces)
     *
     * @param array $hostsList The list of hosts to add
     * @return string
     */
    private function addHosts(array $hostsList = []): string {
      // Check if hosts are set
      // If not, return now
      if(empty($hostsList)) return '';

      // Add each host to a string
      $hosts = '';
      $lastHost = end($hostsList);
      foreach($hostsList as $index => $host) {
        if(!empty($host)) {
          $hosts .= $host;
          if($host !== $lastHost) $hosts .= ' ';
        }
      }

      // Return our hosts string
      return $hosts;
    }
  }
