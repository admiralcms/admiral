<?php
  namespace Admiral\Admiral\Controller\Component;

  use Cake\Controller\Component;
  use Cake\ORM\TableRegistry;

  class UserComponent extends Component {
    private $UserDetails;
    private $Users;
    private $Auth;

    public function __construct($registry, array $config = []) {
      parent::__construct($registry, $config);

      $this->Users = TableRegistry::get('Admiral/Admiral.Users');
      $this->UsersDetails = TableRegistry::get('Admiral/Admiral.UsersDetails');
      $this->Auth = $this->getController()->Auth;
    }

    public function authenticate() {
      $user = $this->Auth->identify()['id'];
      if(!$user) {
        return false;
      }

      // User has been logged in
      $this->Auth->setUser(
        $this->Users
          ->findById($user)
          ->contain([
            'Roles' => [
              'Permissions'
            ]
          ])
          ->first()
      );

      return true;
    }

    public function loadDetails() {
      // Check if a user was found
      if(!$this->Auth->user()) {
        return;
      }

      // Load the user's details from the database and add it to the entity
      $this->Auth->user()->user_details = $this->UsersDetails->find('all',['conditions' => ['id' => $this->Auth->user('id')]])->first();
    }
  }