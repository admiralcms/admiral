<?php
namespace Admiral\Admiral\Controller;

use Cake\I18n\Time;
use Cake\Event\EventInterface;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Http\Cookie\Cookie;
use Cake\Core\{
  Plugin,
  Configure
};
use Admiral\Admiral\Controller\AppController;
use Admiral\Admiral\Form\{
  LoginForm,
  UserDetailsForm,
  ChangePasswordForm
};
use Admiral\Admiral\{
  User,
  Permission
};

class AdminController extends AppController {
  public function beforeFilter(EventInterface $event) {
    $this->Auth->allow(['index','login']);
    $this->Auth->autoRedirect = false;
  }

  public function initialize(): void {
    parent::initialize();

    // Load required models
    $this->loadModel('Admiral/Admiral.Users');
    $this->loadModel('Admiral/Admiral.UsersDetails');
    $this->loadModel('Admiral/Admiral.Options');
    $this->loadModel('Admiral/Admiral.AuthTokens');

    // Check whether the user is logged in or not
    if(!User::get()) {
      // User is not logged in
      // Check whether we are on the login page
      if($this->request->getParam('action') != 'login') {
        // We are not on the login page
        // Redirect the user to the login page
        $this->redirect([
          'controller' => 'Users',
          'action' => 'login',
          'redir' => Router::url([
            'controller' => $this->request->params['controller'], 
            'action' => $this->request->params['action']
          ])
        ]);
      }
    }

    // Check if the user has the right permission
    if(!Permission::check('admiral.admiral.cms.access', 1)) {
      // User does not have the right permission
      // Redirect to the general my-account page
      $this->redirect([
        'controller' => 'Users',
        'action' => 'my_account',
        'my-account'
      ]);
    }
    
    $this->viewBuilder()->setLayout('admin'); # Change the layout to the admin_login layout
  }

  public function index() {    
    $this->set('title', __d('Admiral/Admiral','Dashboard'));
    $this->set('cells', Configure::read('Admiral\Admiral.dashboardCells'));
  }
}
