<?php
  namespace Admiral\Admiral\Controller\Base;

  use Admiral\Admiral\Controller\AppController;
  use Admiral\Admiral\Form\{
    ResetPasswordForm,
    ForgotPasswordForm,
    ChangePasswordForm,
    UserDetailsForm,
    RegisterForm,
    LoginForm
  };
  use Admiral\Admiral\Email;
  use Admiral\Admiral\GraphQL\Datasource\WebauthnDatasource;
  use Cake\I18n\Time;
  use Cake\Utility\Security;
  use Cake\Routing\Router;
  use Cake\Event\Event;
  use Cake\Http\Cookie\Cookie;
  use Cake\ORM\TableRegistry;

  class UsersBaseController extends AppController {
    public function beforeFilter(Event $event) {
      $this->Auth->allow(['login','logout','register','myAccount','forgotPassword', 'resetPassword']);
    }

    public function initialize(): void {
      parent::initialize();

      $this->loadModel('Admiral/Admiral.Users');
      $this->loadModel('Admiral/Admiral.UsersDetails');
      $this->loadModel('Admiral/Admiral.ResetTokens');
      $this->loadModel('Admiral/Admiral.AuthTokens');
      $this->loadComponent('Admiral/Admiral.AutoLogin');
      $this->loadComponent('Admiral/Admiral.User');
    }

    public function register() {
      $register = new RegisterForm();
      $this->set('register', $register);

      if($this->request->is('post')) {
        if($this->Users->findByEmail($this->request->getData('email'))->first()) {
          return $this->Flash->error(__d('Admiral/Admiral','Email is already taken'));
        }
        
        if($this->Users->findByUsername($this->request->getData('username'))->first()) {
          return $this->Flash->error(__d('Admiral/Admiral','Username is already taken'));
        }
      

        $user = $this->Users->newEntity([
          'username' => $this->request->getData('username'),
          'email' => $this->request->getData('email'),
          'password' => $this->request->getData('password'),
          'created' => date("Y-m-d H:i:s"),
          'roles' => [
            ['id' => 2]
          ],
          'users_detail' => [
            'firstname' => '',
            'lastname' => ''
          ]
        ]);

        if($this->Users->save($user)) {
          $this->Flash->success(__d('Admiral/Admiral','Registration success!'));
          return $this->redirect(['controller' => 'Users', 'action' => 'login', 'login']);
        }
        return $this->Flash->error(__d('Admiral/Admiral','Unable to add the user.'));
      }
    }

    public function login() {
      $this->loadComponent('AutoLogin');
      if($this->AutoLogin->login($this->request->getCookie('remember_me'))) {
        // Check if the redir param is empty or not
        if(!empty($this->request->getQuery('redir'))) {
          // Redit param is not empty
          // Redirect the user to it's location
          return $this->redirect();
        }
        
        return $this->redirect($this->referer('/', true));
      }
      
      $login = new LoginForm();
      $this->set('login', $login);
      if($this->request->is('post')) {
        // Login the user
        if(!$this->User->authenticate()) {
          return $this->Flash->error(__d('Admiral/Admiral','Invalid username or password, try again'));
        }

        // Check if RememberMe is set        
        if($this->request->data['remember_me']) {
          // Add the autologin tokens
          $tokenData = $this->AutoLogin->addToken($this->Auth->user('id'));

          // Store token in cookie
          if($tokenData['success']) {
            $this->response = $this->response->withCookie(new Cookie(
              'remember_me',
              $tokenData['token'],
              $tokenData['expires'],
              '/',
              '',
              false,
              true
            ));
          }
        }

        // Check whether the redir param is empty or not
        if(!empty($this->request->getData('redir'))) {
          // redit param is not empty
          return $this->redirect($this->request->getData('redir'));
        }

        // Redirect the user to the redirectUrl
        return $this->redirect($this->referer('/', true));
      }
    }

    public function forgotPassword() {
      // Load the forms
      $forgotPasswordForm = new ForgotPasswordForm();
      $this->set('forgotPasswordForm', $forgotPasswordForm);

      // Check if POST
      if ($this->request->is('post')) {
        // Check if a user exists with given email
        $user = $this->Users->findByEmail($this->request->getData('email'))->first();
        if(!$user) {
          // User does not exist
          return $this->Flash->error(__d('Admiral/Admiral','We could not find a user with the given email address'));
        }

        // Generate random string as token
        $token = Security::randomString(64);
        $user->resettoken = $token;

        // Insert the token into the database
        $tokenEntity = $this->ResetTokens->newEntity();
        $tokenEntity->user_id = $user->id;
        $tokenEntity->token = $token;
        $tokenEntity->created = date("Y-m-d H:i:s");
        if(!$this->ResetTokens->save($tokenEntity)) {
          return $this->Flash->error(__d('Admiral/Admiral','Something went wrong'));
        }

        $email = new Email();
        $email->set('to', $this->request->getData('email'));
        $email->set('subject', 'Request to reset your password');
        $email->set('template', 'forgot-password');
        $email->set('format', 'html');
        $email->set('viewVars', [
          'username' => $user->username, 
          'token' => $token, 
          'url' => Router::url([
            'controller'=>'Users',
            'action'=>'reset_password'
          ], true)
        ]);

        // Try to send the mail
        if(!$email->send()) {
          return $this->Flash->error(__d('Admiral/Admiral','There was an issue sending your request. Please try again later!'));
        }
        
        // Everything went right
        $this->Flash->success(__d('Admiral/Admiral','Check your mailbox for the link to reset your password!'));
      }
    }

    public function resetPassword() {
      // Load the forms
      $resetPasswordForm = new ResetPasswordForm();
      $this->set('resetPasswordForm', $resetPasswordForm);

      // Check if POST
      if ($this->request->is('post')) {
        // Request is POST
        // Get the token entity
        $tokenEntity = $this->ResetTokens->findByToken($this->request->getData('token'))->first();
        if(!$tokenEntity) {
          // Token is invalid
          return $this->Flash->error(__d('Admiral/Admiral','The reset token you\'ve used was invalid'));
        }

        // Check if the token has expired or not
        $time = new Time($tokenEntity->created);
        if($time->addHours(1) >= Time::now()) {
          // Token has expired
          return $this->Flash->error('The reset token you\'ve used has expired!');
        }

        // Check if the user associated with the token exists
        $user = $this->Users->findById($tokenEntity->user_id)->first();
        if(!$user) {
          // User does not exist
          return $this->Flash->error(__d('Admiral/Admiral','Something went wrong while trying to change the password'));
        }

        // Change the password
        $user->password = $this->request->getData('password');
        if(!$this->Users->save($user)) {
          // An error occurred
          return $this->Flash->error(__d('Admiral/Admiral','Something went wrong while trying to change the password'));
        }

        // Delete the token
        if(!$this->ResetTokens->delete($tokenEntity)) {
          $this->log("Could not delete the reset token");
        }

        $this->Flash->success(__d('Admiral/Admiral','Password changed successfully!'));
      }
    }

    public function myAccount($id) {
      // Load the forms
      $changePasswordForm = new ChangePasswordForm();
      $userDetailsForm = new UserDetailsForm();

      // Check if POST
      if ($this->request->is('post')) {
        // Check what action is being send by the form
        switch($this->request->getData('action')){
          case "userDetails": {
            if($userDetailsForm ->execute($this->request->getData())){
              $current_user_data = $this->UsersDetails
                ->find()
                ->where(['id'=> $id])
                ->first();
              $user_data = $this->UsersDetails->patchEntity($current_user_data,['firstname' => $this->request->getData('firstname'), 'lastname' => $this->request->getData('lastname')]);
              if($this->UsersDetails->save($user_data)){
                $this->Flash->success(__d('Admiral/Admiral','Your details where updated successfully'),['key' => 'user_details']);
              }else{
                $this->Flash->error(__d('Admiral/Admiral','There was an issue updating details'),['key' => 'user_details']);
              }
            }else{
              $this->Flash->error(__d('Admiral/Admiral','There was an issue updating details'),['key' => 'user_details']);
            }
            break;
          }
        }
      }

      // Load the default values for the forms
      $userData = $this->Users
        ->find()
        ->where(['Users.id' => $id])
        ->contain(['UsersDetails','Roles'])
        ->first();

      // Get all FIDO2 tokens for user
      $webautnnDatasource = new WebauthnDatasource();
      $keys = $webautnnDatasource->getAllForUser();

      $this->set('userData',$userData);
      $this->set('userDetailsForm', $userDetailsForm);
      $this->set('changePasswordForm', $changePasswordForm);
      $this->set('authenticationKeys', $keys);
    }

    public function logout() {
      // Get the token from cookie
      $cookie = $this->request->getCookie('remember_me');

      // Check if a cookie has been found
      if($cookie){
        // Delete token from db
        $entity = $this->AuthTokens->findByToken($cookie)->first();
        if($entity){
          $this->AuthTokens->delete($entity);
        } 
      }

      // Expire the cookie
      $this->response = $this->response->withExpiredCookie('remember_me');

      // Log the user out and redirect to the login page
      $this->Auth->logout();
      $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }
  }