<?php
namespace Admiral\Admiral;

use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Symfony\Component\Yaml\Yaml;

use Admiral\Admiral\User;

class Permission {
  private static $permissions = [];

  /**
   * Load the permission configuration.
   * This should be located at the "config/permissions.yaml" of the app.
   * 
   * After loading, insert all defined roles in the database to link to users.
   * 
   * @return void
   */
  public static function load(): void {
    // Get a file handle
    $file = new File(CONFIG . DS . 'permissions.yaml');
    
    // Make sure the file exists
    if(!$file->exists()) throw new \Exception('Could not find permissions definition at "' . $file->path . '"');

    // Read the Yaml
    self::$permissions = Yaml::parse($file->read());

    // Load the RolesTable
    $rolesTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.Roles');
    
    // Loop over each role
    // Make sure they exist in the database
    foreach(self::$permissions as $role => $config) {
      // Skip roles that already exist
      if($rolesTable->exists(['name' => $role])) continue;

      // Create a new entity
      $entity = $rolesTable->newEntity([
        'name' => $role,
      ]);

      // Save the entity
      $rolesTable->save($entity);
    }
  }

  /**
   * List all roles currently defined.
   * 
   * @return array
   */
  public static function roles(): array {
    return array_keys(self::$permissions);
  }

  /**
   * List all permissions defined for a role
   * 
   * @return array
   */
  public static function list(string $role): array {
    if(!array_key_exists($role, self::$permissions)) throw new \Exception('Role "' . $role . '" does not exist!');

    return self::$permissions[$role]['permissions'];
  }

  /**
   * Updates the roles for the currently logged in user
   * 
   * @return void
   */
  public static function update(): void {
    // Get the user from the session
    $user = User::get();

    // Get the required tables
    $UsersRolesTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.UsersRoles');
    $rolesTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.Roles');

    // Get the current roles for the user
    $userRoles = $UsersRolesTable->find()->where(['user_id' => $user->id])->all();

    // Get the roles entities
    $roles = [];
    foreach($userRoles as $role) {
      $roles[] = $rolesTable->find()->where(['id' => $role->role_id])->first();
    }

    // Update the user
    $user->roles = $roles;
    User::set($user);
  }

  /**
   * Checks whether the user a permission
   * By default return a variety of integer values:
   * - 1: user has permission
   * - 0: user lacks permission
   * - -1: user not logged in
   * 
   * Can be used to check against a specific value by specifying it.
   * In this case, function will return a boolean
   * 
   * @param string $permission The permission to check
   * @param int $value Check against a specific value
   * @return int|boolean
   */
  public static function check(string $permission, int $value = null) {
    // Get the user from the session
    $user = User::get();

    // If a specific permission is requested, do the check
    if($value !== null) return self::check($permission) == $value;

    // Return if the user is not logged in
    if(!$user) return -1;

    // Update the roles
    self::update();

    // Loop over each role
    // Return if any role contains the permission
    foreach($user->roles as $role) {
      // Make sure the role exists in our definitions
      if(!array_key_exists($role->name, self::$permissions)) continue;

      // Make sure the role has permissions associated
      if(self::$permissions[$role->name]['permissions'] === null) continue;

      if(in_array($permission, self::$permissions[$role->name]['permissions'])) {
        return 1;
      }
    }

    // User does not have the permission
    return 0;
  }
}