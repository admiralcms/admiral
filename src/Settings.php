<?php
namespace Admiral\Admiral;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Utility\Hash;

class Settings {
  public static $_items = []; 

  public static function add(array $item) {
    if(!array_key_exists($item['name'], self::$_items)) {
      self::$_items[$item['name']] = [
        'label'=> Inflector::humanize($item['label']),
        'options'=> []
      ];
    }

    self::$_items[$item['name']]['options'][] = $item['options'];
  }

  public static function saveOption(string $option, string $value) : bool {
    $optionsTable = TableRegistry::get('Admiral/Admiral.Options');
    $option = $optionsTable->findByName($option)->first();
    $option->value = $value;
    return !(empty($optionsTable->save($option)));
  }

  public static function getOptionValue(string $name) {
    $optionsTable = TableRegistry::get('Admiral/Admiral.Options');
    $option = $optionsTable->findByName($name)->first();
    return $option->value;
  }

  public static function getOptions(string $group = null) {
    if($group) {
      return self::$_items[$group];
    }

    return self::$_items;
  }
}