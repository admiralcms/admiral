<?php
  namespace Admiral\Admiral\Webauthn;

  use Cose\Algorithms;
  use Webauthn\PublicKeyCredentialParameters;

  class PublicKey {
    public function getPublicKeyAlgorithms() {
      return [
        new PublicKeyCredentialParameters('public-key', Algorithms::COSE_ALGORITHM_ES256),
        new PublicKeyCredentialParameters('public-key', Algorithms::COSE_ALGORITHM_RS256),
      ];
    }
  }