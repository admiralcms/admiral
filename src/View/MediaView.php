<?php

namespace Admiral\Admiral\View;

use Cake\View\View;

class MediaView extends View {
  public function initialize() {
    parent::initialize();
    
    $this->loadHelper('Filesize');
  }
}
