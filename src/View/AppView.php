<?php
namespace Admiral\Admiral\View;

use Cake\View\View;

class AppView extends View {
  public function initialize() {
    $this->loadHelper('Admiral/Admiral.Ui');
    $this->loadHelper('Admiral/Admiral.WebauthnKey');
  }
}
