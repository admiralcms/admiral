<?php
namespace Admiral\Admiral\View\Helper;

use Cake\View\Helper;

class WebauthnKeyHelper extends Helper {
  public function fingerprint($id) {
    // Convert our ID to hex
    $hex = \bin2hex($id);

    // Get the first 16 characters
    $sub = \substr($hex, 0, 16);

    // Split the hex ever 2 characters
    $chunks = \chunk_split($sub, 2);

    // Replace linebreaks with a colon
    $fingerprint = \str_replace("\r\n", ':', trim($chunks));

    // Uppercase our fingerprint and return it
    return \strtoupper($fingerprint);
  }
}
