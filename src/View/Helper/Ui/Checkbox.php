<?php
  namespace Admiral\Admiral\View\Helper\Ui;

  class Checkbox {
    public function checkbox(bool $checked = false, array $options = []) {
      $attrs = '';
      foreach($options['attrs'] as $attr => $value) {
        $attrs .= ' ' . $attr . '="' . $value . '"';
      }
  
      if($checked) {
        $attrs .= ' checked';
      }
  
      return '<input type="checkbox" data-toggle="toggle" data-size="xs" class="form-check-input" '.$attrs.' />';
    }
  }