<?php
  namespace Admiral\Admiral\View\Helper\Ui;

  class Card {
    public function start() {
      return '<div class="card rounded-0">';
    }
  
    public function header(string $title, string $ns = '') {
      if(!empty($ns)) {
        $title = __d(strtolower($ns),$title);
      } else {
        $title = __($title);
      }
  
      return '<div class="card-header">' . h($title) . '</div>';
    }
  
    public function bodystart() {
      return '<div class="card-body">';
    }
  
    public function bodyend() {
      return '</div>';
    }
    
    public function end() {
      return '</div>';
    }
  }