<?php
  namespace Admiral\Admiral\View\Helper\Ui;

  use Cake\Utility\Security;

  class Table {
    use \Cake\Log\LogTrait;

    private $_checkAll = false;
    private $_columns = ['max' => 12, 'used' => 0];

    public function start(array $options = []) {
      // Generate a random id if needed
      if(empty($options['id'])) {
        $options['id'] = Security::randomString(32);
      }

      // See if we need to add a checkall box
      if(isset($options['checkall']) && $options['checkall']) {
        $this->_checkAll = true;
        $this->_columns['max'] = 11;
      }

      // Start the output buffer for this table
      ob_start();

      // Start building the table
      ?>
        <table class="table table-striped table-sm" id="table-<?= h($options['id']); ?>">
      <?php
    }

    public function header(array $headers) {
      ?>
        <thead>
          <?php
            // Add checkbox if needed
            if($this->_checkAll):
              $this->checkColumns(1);
              ?>
                <th class="col-auto">
                  <input type="checkbox" data-action="checkall"/>
                </th>
              <?php
            endif;

            // Add all the headers
            foreach($headers as $index => $header):
              if(!empty($header['width'])) {
                $this->checkColumns($header['width']);
                ?>
                  <th class="col-<?= $header['width']; ?>">
                    <?= h($index); ?>
                  </th>
                <?php
                continue;
              }

              $this->checkColumns(1);
              ?>
                <th class="col-auto">
                  <?= h($header); ?>
                </th>
              <?php
            endforeach;
          ?>
        </thead>
      <?php
      // Reset the column counter
      $this->_columns['used'] = 0;
    }

    public function bodystart() {
      ?>
        <tbody>
      <?php
    }

    public function bodyend() {
      ?>
        </tbody>
      <?php
    }

    private function checkColumns(int $width) {
      // Update the column count
      $this->_columns['used'] += $width;

      // Make sure we can't assign too much columns
      if($this->_columns['used'] > $this->_columns['max']) {
        throw new \Exception('Table columns exhausted! (trying to assign ' . $this->_columns['used'] . ' columns out of ' . $this->_columns['max'] . ')');
      }
    }

    public function rowstart($id = null) {
      ?>
        <tr id="<?= h($id); ?>">
      <?php
      if($this->_checkAll):
        ?>
          <td class="col-auto"><input type="checkbox" data-action="checkall"/></td>
        <?php
      endif;
    }

    public function rowend() {
      ?>
        </tr>
      <?php
    }

    public function columnstart() {
      ?>
        <td>
      <?php
    }

    public function columnend() {
      ?>
        </td>
      <?php
    }

    public function end() {
      ?>
        </table>
      <?php

      // Return the table
      //dd(ob_get_clean());
      return ob_get_clean();
    }
  }
