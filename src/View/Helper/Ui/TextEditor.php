<?php
  namespace Admiral\Admiral\View\Helper\Ui;

  use Cake\Utility\Security;

  class TextEditor {
    public function create(string $content = "", array $options = []) {
      if(!empty($options['class'])) {
        $options['class'] .= ' tmce-editor';
      } else {
        $options['class'] = 'tmce-editor';
      }
  
      // Check if an id has been specified
      // Generate one if not
      if(empty($options['id'])) {
        // Generate a random string
        $id = Security::randomString(32);
  
        // Add it as the id of the element
        $options['id'] = 'tmce-editor-' . $id;
      }
  
      // Build the TinyMCE element
      $out = '';
      $out .= '<textarea rows="25"';
      foreach($options as $option => $value) {
        $out .= ' ' . $option . '="' . $value . '"';
      }
      $out .= '>';
      $out .= h($content);
      $out .= '</textarea>';
  
      return $out;
    }
  }