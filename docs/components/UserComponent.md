# UserComponent
The UserComponent has been made to easily authenticate a user and load it's extra data.  

## Logging in a user
Logging in a user can be done by loading the component into your controller and passing the required POST variables.  
Since it uses the `AuthComponent::identify()` method from CakePHP, it requires the same parameters to be send in the post request.  
```php
// Load the component
$this->loadComponent('Admiral/Admiral.User');

if($this->User->authenticate()) {
  // User has been authenticated
}
```