# AutoLogin Component
The AutoLogin component, is, as the name implies, made to automatically log somebody in if a valid AuthToken has been passed.  
This AuthToken is by default send as the `remember_me` cookie but can be used in different ways as well.

# Automatically logging in a user
In order to automatically login a user, we just need to load the component and call the method.  
The method will respond either `false` or with `UserEntity Object` depending on whether the token was valid:
```php
use Admiral\Admiral\User;

public function initialize() {
  $this->loadComponent('Admiral/Admiral.AutoLogin');

  if(!User::get()) {
    $user = $this->AutoLogin->login('myToken');
    
    if($user) {
      // User was found
    } else {
      // No user was found
    }
  }
}
```