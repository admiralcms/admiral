# Dashboard Component
The [Dashboard component](https://gitlab.com/AdmiralCMS/Admiral/blob/master/src/Controller/Component/DashboardComponent.php) is used to add items to the dashboard page.  

# Adding a dashboard item
Dashboard items work by adding [View Cells](https://book.cakephp.org/3.0/en/views/cells.html) to the output.  
So first, we have to [create a cell](https://book.cakephp.org/3.0/en/views/cells.html#creating-a-cell) in order to add our Dashboard item:
```php
// src/View/Cell/MyCell.php
namespace App\View\Cell;

use Cake\View\Cell;

class ExampleCell extends Cell {
  public function display() {
    $this->set('message', 'Hello World!');
  }
}
```
And create the appropriate template for it:
```html
<!-- src/Template/Cell/Example/display.ctp -->
<div class="text-center">
  <?= h($message); ?>
</div>
```

We can then add our new cell to the dashboard by adding the following code to the `bootstrap.php`:
```php
use Cake\Controller\Controller;

$controller = new Controller();

$controller->loadComponent('Admiral/Admiral.Dashboard');

$controller->Dashboard
->registerItem([
  'title' =>'Example Dashboard Item',
  'ns' => 'App',
  'cell' => 'Example'
]);
```