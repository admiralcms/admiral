# SecurityHeaderComponent
The SecurityHeaderComponent has been made to easily set headers related to security and privacy on clients.
It currently has support for the following headers:
- [`Strict-Transport-Security`](#strict-transport-security) ([MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security))
- [`Referrer-Policy`](#referrer-policy) ([MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy))
- [`X-Frame-Options`](#x-frame-options) ([MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options))
- [`X-Content-Type-Options`](#x-content-type-options) ([MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options))
- [`Permissions-Policy`](#permissions-policy) ([MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy))
- [`Content-Security-Policy`](#content-security-policy) ([MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy))

## Strict-Transport-Security
The `Strict-Transport-Security` header, more commonly referred to as `HSTS` let's a website inform browsers that they should only access the website through `HTTPS` as opposed to `HTTP`.  
This means that it'll be significantly more difficult for an attacker to start a `Man-in-the-Middle` attack when a user first visits a website.  
Google maintains [a list](https://hstspreload.org/) of websites that support `HSTS` that get hardcoded into `Chrome` so that browsers will always use `HTTPS` and never fall back to `HTTP`.  
This list is also used in other common browsers such as `Firefox` and `Opera`.  
Setting up `HSTS` is a good practice to keep your users from accidentally using the insecure `HTTP` protocol when loading your website or sending data to it.  
While it is recommended you add your website to this list, not doing so but setting the `Strict-Transport-Security` header will still inform a browser to use `HTTPS` by default next time the browser sends a request.

You can let Admiral handle setting up the appropriate headers for HSTS by adding this to your `AppController`'s `beforeShutdown` method:
```php
class AppController extends Controller {
  public function shutdownProcess() {
    $this->loadComponent('Admiral/Admiral.SecurityHeader');
    $this->SecurityHeader->setHsts(true, false, true, 63072000);
  }
}
```

It takes 4 arguments in total:
| Argument | Type | Default | Optional | Comment |
|----------|------|---------|----------|---------|
| $enabled | `bool` | `true` | Yes | Enables (true) or disables (false) the setting of `Strict-Transport-Security` |
| $includeSubDomains | `bool` | `false` | Yes | Whether to enable HSTS for subdomains |
| $preload | `bool` | `true` | Yes | Whether to consult the list of HSTS enabled websites |
| $expirty | `int` | `63072000` | Yes | Change the expiry time for HSTS |

## Referrer-Policy
The `Referrer-Policy` header tells the browser how much referrer information is sent via the [referrer header](https://developer.mozilla.org/en-US/docs/Web/Security/Referer_header:_privacy_and_security_concerns).  
Setting this header can increase privacy (as well as security when navigating from sensitive pages) for the visitor by not disclosing as much information (if any at all).  
The drawback is that it can interfere with some analytics software.  
Please visit the [MDN page](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy) for possible values for both the `default` and `fallback` and which will result in what.

You can let Admiral handle setting up the appropriate headers by adding this to your `AppController`'s `beforeShutdown` method:
```php
class AppController extends Controller {
  public function shutdownProcess() {
    $this->loadComponent('Admiral/Admiral.SecurityHeader');
    $this->SecurityHeader->setReferrerPolicy('strict-origin', 'strict-origin-when-cross-origin');
  }
}
```

It takes 2 arguments in total:
| Argument | Type | Default | Optional | Comment |
|----------|------|---------|----------|---------|
| $default | `string` | `strict-origin` | Yes | Target value for the header |
| $fallback | `string` | `strict-origin-when-cross-origin` | Yes | Fallback value for the header when browser does not support `$default` |

## X-Frame-Options
The `X-Frame-Options` header can be used to tell a browser whether a page may be rendered within an `iframe` (or similar elements).  
Sites should use this to avoid [click-jacking attacks](https://developer.mozilla.org/en-US/docs/Web/Security/Types_of_attacks#click-jacking) by disallowing content from being embedded into other sites.  

You can let Admiral handle setting up the appropriate headers by adding this to your `AppController`'s `beforeShutdown` method:
```php
class AppController extends Controller {
  public function shutdownProcess() {
    $this->loadComponent('Admiral/Admiral.SecurityHeader');
    $this->SecurityHeader->setXFrame('SAMEORIGIN');
  }
}
```

It takes 1 argument in total:
| Argument | Type | Default | Optional | Comment |
|----------|------|---------|----------|---------|
| $setting | `string` | `SAMEORIGIN` | Yes | Visit [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options) for more information |

## X-Content-Type-Options
Setting the `X-Content-Type-Options` to `nosniff` disables the browser's mechanic to sniff MIME-types from the content of a file.  
Setting this *does* require the server to set the `Content-Type` header appropriate for the file.

You can let Admiral handle setting up the appropriate headers by adding this to your `AppController`'s `beforeShutdown` method:
```php
class AppController extends Controller {
  public function shutdownProcess() {
    $this->loadComponent('Admiral/Admiral.SecurityHeader');
    $this->SecurityHeader->sniffMime(true);
  }
}
```

It takes 1 argument in total:
| Argument | Type | Default | Optional | Comment |
|----------|------|---------|----------|---------|
| $setting | `bool` | `true` | Yes | Whether to set the `X-Content-Type-Options` header to `nosniff` |

## Permissions-Policy
The `Permissions-Policy` (previously known as `Feature-Policy`) header tells the browser which features it may or may not use on this document as well as any `iframes` (and similar elements) in the document.  
This header works on a `whitelist` basis.  
Please visit the [MDN page](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy) for more information on directives.

You can let Admiral handle setting up the appropriate headers by adding this to your `AppController`'s `beforeShutdown` method:
```php
class AppController extends Controller {
  public function shutdownProcess() {
    $this->loadComponent('Admiral/Admiral.SecurityHeader');

    $this->SecurityHeader
      ->setPermission('interest-cohort')  # Disable Google FLoC
      ->setPermission('fullscreen', 'self');  # Enable fullscreen for self
      ->setPermission('fullscreen', 'https://www.youtube.com');  # Enable fullscreen for YouTube
      ->getPermissions();  # Finalize headers
  }
}
```

Each `setPermission` method takes 2 arguments in total:
| Argument | Type | Default | Optional | Comment |
|----------|------|---------|----------|---------|
| $permission | `string` | N/A | No | The permission directive |
| $host | `string` |  | Yes | The hosts on which to allow this directive (leave empty to deny permission) |

**Note:** Do not forget to run the `getPermissions` method or else your permissions policies **won't** be set in the response!

## Content-Security-Policy
The `Content-Security-Policy` can help mitigate `XSS` attacks by only allowing certain content to run from certain origins.  
It is used to limit the origins from which a certain asset will be executed.  
Please visit the [MDN page](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy) for more information on directives.

You can let Admiral handle setting up the appropriate headers by adding this to your `AppController`'s `beforeShutdown` method:
```php
class AppController extends Controller {
  public function shutdownProcess() {
    $this->loadComponent('Admiral/Admiral.SecurityHeader');
    $this->SecurityHeader
      ->setCsp('default-src', 'self')
      ->setCsp('img-src', 'self')
      ->setCsp('script-src', 'self')
      ->setCsp('style-src', 'self')
      ->getContentPolicies();
  }
}
```

Each `setCsp` method takes 2 arguments in total:
| Argument | Type | Default | Optional | Comment |
|----------|------|---------|----------|---------|
| $directive | `string` | N/A | No | The directive which to enforce |
| $host | `string` |  | Yes | The hosts on which to allow this directive |

**Note:** Do not forget to run the `getContentPolicies` method or else your policies **won't** be set in the response!