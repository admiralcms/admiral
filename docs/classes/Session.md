# Session
###### `class` Admiral\Admiral\Session
The Session class is made to access sessions in order to read, write and delete certain values.  
The inner workings are identical to `\Cake\Routing\Router::getRequest()->getSession()`.  
For more information, refer to the [CakePHP Cookbook](https://book.cakephp.org/3/en/development/sessions.html).

## Usage

### Reading from session
```php
use Admiral\Admiral\Session;

$value = Session::get()->read('MyVariable');
```

### Writing to session
```php
use Admiral\Admiral\Session;

Session::get()->write('MyVariable', $myValue);
```