# GraphQL
Admiral uses GraphQL as it's API provided by the [`Admiral/GraphQL` package](https://gitlab.com/admiralcms/graphql).  
If you're not familiar with GraphQL, please visit the [GraphQL Website](https://graphql.org/) for more information.  
This documentation will only go into how to register new types and resolvers for the GraphQL.  
GraphQL APIs are mostly self-documenting, so for the types available, please use a GraphQL client like [Insomnia](https://insomnia.rest/) to view these documentations.

For more information, please refer to the documentation for [`Admiral/GraphQL`](https://gitlab.com/admiralcms/graphql/-/tree/master/docs).