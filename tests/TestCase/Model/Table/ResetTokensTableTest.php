<?php
namespace Admiral\Admiral\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Admiral\Admiral\Model\Table\ResetTokensTable;

/**
 * Admiral\Admiral\Model\Table\ResetTokensTable Test Case
 */
class ResetTokensTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admiral\Admiral\Model\Table\ResetTokensTable
     */
    public $ResetTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Admiral/Admiral.ResetTokens',
        'plugin.Admiral/Admiral.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ResetTokens') ? [] : ['className' => ResetTokensTable::class];
        $this->ResetTokens = TableRegistry::getTableLocator()->get('ResetTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ResetTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
