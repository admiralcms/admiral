<?php
namespace Admiral\Admiral\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Admiral\Admiral\Model\Table\RolesPermissionsTable;

/**
 * Admiral\Admiral\Model\Table\RolesPermissionsTable Test Case
 */
class RolesPermissionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admiral\Admiral\Model\Table\RolesPermissionsTable
     */
    public $RolesPermissions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Admiral/Admiral.RolesPermissions',
        'plugin.Admiral/Admiral.Roles',
        'plugin.Admiral/Admiral.Permissions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RolesPermissions') ? [] : ['className' => RolesPermissionsTable::class];
        $this->RolesPermissions = TableRegistry::getTableLocator()->get('RolesPermissions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RolesPermissions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
