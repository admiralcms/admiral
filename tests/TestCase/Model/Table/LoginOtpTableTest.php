<?php
namespace Admiral\Admiral\Test\TestCase\Model\Table;

use Admiral\Admiral\Model\Table\LoginOtpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admiral\Admiral\Model\Table\LoginOtpTable Test Case
 */
class LoginOtpTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admiral\Admiral\Model\Table\LoginOtpTable
     */
    public $LoginOtp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Admiral/Admiral.LoginOtp',
        'plugin.Admiral/Admiral.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LoginOtp') ? [] : ['className' => LoginOtpTable::class];
        $this->LoginOtp = TableRegistry::getTableLocator()->get('LoginOtp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LoginOtp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
