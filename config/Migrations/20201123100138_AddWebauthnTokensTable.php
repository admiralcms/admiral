<?php
use Migrations\AbstractMigration;

class AddWebauthnTokensTable extends AbstractMigration {
  public function change() {
    $table = $this->table('webauthntokens')
      ->addColumn('user_id', 'integer', ['null' => false,'signed' => true])                 
      ->addColumn('tokenname', 'string', ['null' => false])
      ->addColumn('credential_id', 'string', ['null' => false,])
      ->addColumn('data', 'text', ['null' => false,])
      ->addColumn('created', 'datetime', ['null' => false,])
      ->addIndex(['user_id'], ['unique' => false, 'name' => 'id_auth_tokens_user_id'])
      ->addForeignKey('user_id','users','id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
      ->Save();
  }
}
