<?php

use Migrations\AbstractMigration;

class CreateAuthTokensTable extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   * @throws \RuntimeException
   * @throws \InvalidArgumentException
   */
  public function change() {
    $table = $this->table('auth_tokens')                   
      ->addColumn('token', 'string', ['null' => false])                      
      ->addColumn('created', 'datetime', ['null' => false,])
      ->addColumn('modified', 'datetime', ['null' => false,])
      ->addColumn('expires', 'datetime', ['null' => false,])
      ->addColumn('user_id', 'integer', ['null' => false,'signed' => true])
      ->addIndex(['token'], ['unique' => false,'name' => 'id_auth_tokens_token'])
      ->addIndex(['user_id'], ['unique' => false, 'name' => 'id_auth_tokens_user_id'])
      ->addForeignKey('user_id','users','id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
      ->Save();        
  }
}