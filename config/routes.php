<?php
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {  
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Routes for the CMS Dashboard
 */

 Router::plugin('Admiral/Admiral',['path'=>'/admin'],function($routes){
  $routes->connect('/',['controller' => 'Admin', 'action' => 'index']);
  $routes->connect('/login',['controller' => 'Users', 'action' => 'login']);
  $routes->connect('/logout',['controller' => 'Users', 'action' => 'logout']);
  $routes->connect('/profile',['controller' => 'Users', 'action' => 'profile']);
  $routes->connect('/users',['controller' => 'Users', 'action' => 'index']);
  $routes->connect('/users/edit/:id',['controller' => 'Users', 'action' => 'edit'])
    ->setPass(['id','lang']);
    $routes->connect('/users/getrights/:id',['controller' => 'Users', 'action' => 'getrights'])
    ->setPass(['id']);
  $routes->connect('/settings',['controller' => 'Settings', 'action' => 'index']);

  $routes->connect('/media/library',['controller' => 'Media', 'action' => 'index']);
  $routes->connect('/media/add',["controller" => "Media","action"=>"add"]);
  $routes->connect('/media/delete',["controller" => "Media","action"=>"delete"]);
  $routes->get('/keepalive', ['controller' => 'Keepalive', 'action' => 'index']);

  $routes->fallbacks(DashedRoute::class);
 });